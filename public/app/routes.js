var app =  angular.module('appRoutes',['ngRoute'])
.config(function($routeProvider,$locationProvider) {
	$routeProvider
	.when('/',{
		templateUrl:'app/views/pages/home.html'
	})

	.when('/about',{
		templateUrl: 'app/views/pages/about.html'
	})

	.when('/services',{
		templateUrl: 'app/views/pages/services.html'
	})

	.when('/team',{
		templateUrl: 'app/views/pages/team.html',
		controller:'teamShowCtrl'
	})

	.when('/contact',{
		templateUrl: 'app/views/pages/contact.html',
		controller: 'contactCtrl',
		controllerAs: 'email'
	})

	.when('/careers',{
		templateUrl: 'app/views/pages/careers.html'
	})

	.when('/survey',{
		templateUrl: 'app/views/pages/survey.html'
	})

	.when('/thank',{
		templateUrl: 'app/views/pages/thank.html'
	})

	.when('/admin',{
		templateUrl: 'app/views/pages/admin/login.html',
		controller: 'admincontroller'
	})
	.when('/logout',{
		templateUrl: 'app/views/pages/admin/logout.html',
		authenticated: true
	})

	.when('/subs',{
		templateUrl: 'app/views/pages/admin/subs.html',
		controller:  'subsCtrl'
	})

	.when('/blog',{
		templateUrl: 'app/views/pages/blog.html',
		controller:  'blogShowCtrl'
	})

	.when('/blog-edit',{
		templateUrl: 'app/views/pages/admin/blog-edit.html',
		controller:  'blogCtrl',
		controllerAs: 'blogger',
		authenticated: true
	})

	.when('/team-edit',{
		templateUrl: 'app/views/pages/admin/team-edit.html',
		controller:  'teamctrl',
		controllerAs: 'member',
		authenticated: true
	})
	
	.otherwise({ redirectTo: '/'});

	$locationProvider.html5Mode({
		enabled: true,
		requireBase:false
	});
});
app.run(['$rootScope','Auth','$location',function($rootScope,Auth,$location){
	$rootScope.$on('$routeChangeStart',function(event,next,current){

		if(next.$$route.authenticated == true){
			if(!Auth.isLoggedIn()){
				event.preventDefault();
				$location.path('/')
			}
		}else if(next.$$route.authenticated == false){
			// console.log('should not be authenticated');
		} 
	
	});
}]);
