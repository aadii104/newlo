angular.module('blogServices', [])

.factory('Blog',function ($http) {
        blogFactory = {};

        blogFactory.add = function(newBlog){
                return $http.post('/api/blog',newBlog)
        }

        blogFactory.getBlog =  function(){
                return $http.get('/api/blog/');
        }


        return blogFactory;
});

