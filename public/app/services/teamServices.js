angular.module('teamServices', [])

.factory('Team',function ($http) {
        teamFactory = {};

        teamFactory.add = function(newMember){
                return $http.post('/api/team/',newMember)
        }

        teamFactory.getMember =  function(){
                return $http.get('/api/team/');
        }


        return teamFactory;
});
