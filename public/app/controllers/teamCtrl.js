var app = angular.module('teamctrl',['teamServices'])
.controller('teamctrl', function ($http,$scope,Team) {


    $scope.addMember = function(newMember){
        $scope.loading  = true;
        $scope.errorMsg = false;

        Team.add($scope.newMember).then(function(data){
            console.log(data.data.success);
            console.log(data.data.message);
            console.log(data);
            if(data.data.success){
                $scope.loading  = false;
                $scope.successMsg = data.data.message;
            }else{
                $scope.loading  = false;
                $scope.errorMsg   =  data.data.message;
            }
        });
    };

})
.controller('teamShowCtrl',function(Team,$scope){
    $scope.errorMsg = false;   
    function getMember(){ 
        Team.getMember().then(function (data) {
        if(data.data.success){
            console.log(data.data);
        }else {
            $scope.list = data;
            // console.log($scope.list);
            var new_data = data.data;
            // console.log(new_data);
            $scope.new_data = new_data.map(function(value){
            return { member_name: value.member_name, member_email: value.member_email, member_position: value.member_position,
                member_post: value.member_post, member_desc: value.member_desc };
                });

            console.log(new_data);
        }
    });
  }
  getMember()

  });