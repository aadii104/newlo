angular.module('admincontroller',['authServices'])
.controller('admincontroller', function (Auth,$timeout, $location,$rootScope,$scope) {

	$scope.loadme = false;
	$scope.loginData = {};

	$rootScope.$on('$routeChangeStart',function(){
		//console.log(isLoggedIn);
		if(Auth.isLoggedIn()){
			console.log('success: user isLoggedIn in');
			$scope.isLoggedIn = false;
			Auth.getUser().then(function(data){
				$scope.username = data.data.username;
				$scope.loadme = true;
			});
		}else{
			console.log('success: not log in');
			$scope.username = "";
			$scope.loadme = true;
			$scope.isLoggedIn = true;
	 }

	});

	$scope.doLogin = function() {
		console.log($scope.loginData);
		$scope.loading  = true;
		$scope.errorMsg = false;

		Auth.login($scope.loginData).then(function(data) {
			console.log(data);
			if(data.data.success){

				$scope.loading  = false;
				$scope.successMsg = data.data.message + "....redirecting";

				$timeout(function() {
					$location.path('/thank');
					$scope.loginData = "";
					$scope.successMsg = false;
				}, 2000);
			}else{
				$scope.loading  = false;
				$scope.errorMsg   = data.data.message;

			}
		});
	};

	$scope.logout = function() {
		Auth.logout();
		$location.path('/logout');
		$timeout(function() {
			$location.path('/');
		}, 2000);
	};

});
