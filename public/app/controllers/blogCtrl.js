angular.module('blogCtrl',['blogServices'])
.controller('blogCtrl', function ($http,$scope,Blog) {


    $scope.addBlog = function(newBlog){
        $scope.loading  = true;
        $scope.errorMsg = false;

        Blog.add($scope.newBlog).then(function(data){
            console.log(data.data.success);
            console.log(data.data.message);

            if(data.data.success){
                $scope.loading  = false;
                $scope.successMsg = data.data.message;
            }else{
                $scope.loading  = false;
                $scope.errorMsg   =  data.data.message;
            }



        });
    };

})
.controller('blogShowCtrl',function(Blog,$scope){
    $scope.errorMsg = false;   
    
    function getBlog(){ 
    Blog.getBlog().then(function (data) {
      if(data.data.success){
        console.log(data.data);
        // $scope.blogs = data.data.blogs;
      }else {
        $scope.list = data;
        // console.log($scope.list);
        var new_data = data.data;
        // console.log(new_data);
        $scope.new_data = new_data.map(function(value){
        return { title: value.title, author: value.author, content: value.content };
             });

           console.log(new_data);
      }
    });
  }
  getBlog()

  });

