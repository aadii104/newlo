var mongoose 		= require('mongoose');
var Schema   		= mongoose.Schema;


var TeamSchema    = new Schema({
 member_name:       {type: String, required:true},
 member_email:      {type: String, required:true},
 member_position:   {type: String, required:true},
 member_post:       {type: String, required:true},
 member_desc:       {type: String, required:true}

});



module.exports   = mongoose.model('Team', TeamSchema);
