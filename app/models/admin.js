var mongoose 		= require('mongoose');
var Schema   		= mongoose.Schema;
var bcrypt 			= require('bcrypt-nodejs'); 

 var AdminSchema    = new Schema({
 	username:          {type: String, required:true},
 	password:          {type: String, required:true} 
 });


AdminSchema.pre('save', function (next) {
	  var admin  = this;
	  bcrypt.hash(admin.password,null,null,function (err,hash) {
	  	 if(err){return next(err);}
	  	 admin.password = hash;
	  	 next();
	  });
});

AdminSchema.methods.comparePassword = function(password) {
	 return bcrypt.compareSync(password,this.password);
};





 module.exports   = mongoose.model('admin', AdminSchema);